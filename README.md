LAMP Stack Vagrant box
=

Provisioned using Ansible, config in /vagrant

**This config:**

- Ubuntu 16.04 
- Apache
- Mysql
- PHP 7.2

Set up
--

Run the `vagrant up` command!

Had some is issues creating a ssh tunnel using password authentication with this vagrant box. 
If this happens, use key pair config and point it at private key in the .vagrant file

Config
-
Settings are located in `/vagrant/main`. 